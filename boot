#!/usr/bin/env bash

# NB! Assuming run from ../

# Make main script easy accessible
ln -s `pwd`/fenics-scripts/fenics fenics

# Get all repositories
./fenics-scripts/getrepos

# Make links to upd scripts
./fenics-scripts/makeupdlinks

