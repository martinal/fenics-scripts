What
====
A set of personal FEniCS workflow scripts.

Who
===
Written by Martin Sandve Alnæs.
Use at own risk.

How
===
To set up initially, do:

    mkdir <...>/fenics
    cd <...>/fenics
    git clone git@bitbucket.org:martinal/fenics-scripts
    ./fenics-scripts/boot

This gets all project repositories and checks out branches.

Some environment variables are also assumed,
currently not documented in this repository (TODO!).

